package test.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import application.controller.Controller;
import application.model.Prisliste;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Transaktion;
import application.model.TransaktionsLinje;

public class TestSetPrisPaaTransaktionLinje {

	private Transaktion transaktion;
	private Produktgruppe produktgruppe;
	private Produkt produkt;
	private Prisliste prisliste;
	private TransaktionsLinje transaktionsLinje;

	@Before
	public void setUp() {
		transaktion = new Transaktion("Ole", "123");
		produktgruppe = new Produktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		produkt = new Produkt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
		prisliste = new Prisliste("Fredagsbar");
		transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);

	}
	
	@Test
	public void TC1() {
		prisliste.opretPrislisteLinje(36, produkt);
		
		Controller.setPrisPaaTransaktionsLinje(transaktionsLinje, prisliste, transaktion);
		
		assertEquals(transaktionsLinje.getPris(), 36, 0.01);
	}
	
	@Test
	public void TC2() {
		
		try {
			Controller.setPrisPaaTransaktionsLinje(transaktionsLinje, prisliste, transaktion);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Der findes ingen pris til produktet for denne prisliste", e.getMessage());
		}
		assertEquals(transaktionsLinje.getPris(), 0, 0.01);
	}

}
