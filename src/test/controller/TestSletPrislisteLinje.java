package test.controller;

import static org.junit.Assert.*;

import org.junit.Test;

import application.controller.Controller;
import application.model.Prisliste;
import application.model.PrislisteLinje;
import application.model.Produkt;
import application.model.Produktgruppe;

public class TestSletPrislisteLinje {

	@Test
	public void TC1() {
		Produktgruppe produktgruppe = new Produktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		Produkt produkt = new Produkt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
		Prisliste prisliste = new Prisliste("Fredagsbar");
		PrislisteLinje prislisteLinje = prisliste.opretPrislisteLinje(36, produkt);
		assertEquals(1, produkt.getPrislisteLinjer().size());
		assertEquals(1, prisliste.getPrislisteLinjer().size());
		assertEquals(produkt, prislisteLinje.getProdukt());
		assertEquals(prisliste, prislisteLinje.getPrisliste());
		
		Controller.sletPrislisteLinje(prislisteLinje);
		
		assertEquals(0, produkt.getPrislisteLinjer().size());
		assertEquals(0, prisliste.getPrislisteLinjer().size());
		assertEquals(null, prislisteLinje.getProdukt());
		assertEquals(null, prislisteLinje.getPrisliste());
		
		
	}

}
