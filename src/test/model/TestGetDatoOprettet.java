package test.model;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import application.model.Transaktion;

public class TestGetDatoOprettet {

	@Test
	public void TC1() {
		Transaktion t = new Transaktion("Ole", "123");
		
		assertEquals(t.getDatoOprettet(), LocalDate.now());
	}

}
