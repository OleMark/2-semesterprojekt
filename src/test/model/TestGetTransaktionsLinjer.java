package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Transaktion;
import application.model.TransaktionsLinje;

public class TestGetTransaktionsLinjer {

	@Test
	public void TC1() {
		Transaktion transaktion = new Transaktion("Ole", "123");
		Produktgruppe produktgruppe = new Produktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		Produkt produkt = new Produkt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
		
		TransaktionsLinje transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
		
		assertEquals(transaktion.getTransaktionsLinjer().get(0), transaktionsLinje);
		assertEquals(transaktion.getTransaktionsLinjer().size(), 1);
		assertEquals(transaktion.getTransaktionsLinjer().get(0).getProdukt(), produkt);
		assertEquals(transaktion.getTransaktionsLinjer().get(0).getAntal(), 1);
	}
	
	@Test
	public void TC2() {
		Transaktion transaktion = new Transaktion("Ole", "123");
		
		assertTrue(transaktion.getTransaktionsLinjer().isEmpty());
		
		
		assertEquals(transaktion.getTransaktionsLinjer().size(), 0);
	}
	

}
