package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Rabat;
import application.model.Transaktion;

public class TestGetRabatForTransaktion {

	@Test
	public void TC1() {
		Transaktion t = new Transaktion("Ole", "123");
		
		Rabat r = new Rabat(0.1);
		
		t.setRabat(r);
		
		assertEquals(t.getRabat(), r);
	}

	@Test
	public void TC2() {
		Transaktion t = new Transaktion("Ole", "123");
		
		assertEquals(t.getRabat(), null);
	}
}
