package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Transaktion;

public class TestGetMedarbejder {

	@Test
	public void TC1() {
		String medarbejder = "Ole";
		
		Transaktion t = new Transaktion(medarbejder, "123");
		
		assertEquals(medarbejder, t.getMedarbejder());
	}

}
