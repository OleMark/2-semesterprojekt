package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Rabat;
import application.model.Transaktion;

public class TestSetRabatNullITransaktion {

	@Test
	public void TC1() {
		Transaktion transaktion = new Transaktion("Ole", "123");
		Rabat rabat = new Rabat(0.1);
		
		transaktion.setRabat(rabat);
		
		assertEquals(transaktion.getRabat(), rabat);
		
		transaktion.setRabatNull();
		
		assertEquals(transaktion.getRabat(), null);
	}

}
