package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Kunde;
import application.model.Transaktion;

public class TestSetKundeNullITransaktion {

	@Test
	public void TC1() {
		Transaktion transaktion = new Transaktion("Ole", "123");
		Kunde kunde = new Kunde("Hans", "12345678", "hans123@hans.dk");
		
		transaktion.setKunde(kunde);
		
		assertEquals(transaktion.getKunde(), kunde);
		
		transaktion.setKundeNull();
		
		assertEquals(transaktion.getKunde(), null);
	}

}
