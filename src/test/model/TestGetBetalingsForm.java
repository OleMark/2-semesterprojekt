package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Betalingsform;
import application.model.Transaktion;

public class TestGetBetalingsForm {

	@Test
	public void TC1() {
		Transaktion t = new Transaktion("Ole", "123");
		
		Betalingsform b = Betalingsform.IKKEGENNEMFOERT;
		
		assertEquals(t.getBetalingsform(), b);
	}

}
