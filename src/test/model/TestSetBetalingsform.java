package test.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import application.model.Betalingsform;
import application.model.Transaktion;

public class TestSetBetalingsform {
	
	private Transaktion transaktion;
	private Betalingsform betalingsform;

	@Before
	public void setUp() {
		transaktion = new Transaktion("Ole", "123");
		betalingsform = Betalingsform.KONTANT;

	}
	
	@Test
	public void TC1() {
		assertEquals(transaktion.getBetalingsform(), Betalingsform.IKKEGENNEMFOERT);
		
		transaktion.setBetalingsform(betalingsform);
		
		assertEquals(transaktion.getBetalingsform(), Betalingsform.KONTANT);
		
	}
	
	@Test
	public void TC2() {
		transaktion.setBetalingsform(betalingsform);
		
		try {
			transaktion.setBetalingsform(betalingsform);
			fail();
		}
		catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Pre condition violated");
		}
	}

}
