package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Transaktion;

public class TestGetSamletPris {

	@Test
	public void TC1() {
		Transaktion t = new Transaktion("Ole", "123");
		
		t.setPrisTest(100.0);
		
		assertEquals(t.getSamletPris(), 100.0, 0.01);
		
	}

}
