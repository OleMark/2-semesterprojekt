package test.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Transaktion;
import application.model.TransaktionsLinje;

public class TestOpretTransaktionsLinje {

	private Transaktion transaktion;
	private Produktgruppe produktgruppe;
	private Produkt produkt;
	
	@Before
	public void setUp() {
		transaktion = new Transaktion("Ole", "123");
		produktgruppe = new Produktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		produkt = new Produkt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
	}
	
	@Test
	public void TC1() {
		TransaktionsLinje transaktionsLinje = null;
		
		assertEquals(transaktionsLinje, null);
		
		transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
		
		assertEquals(transaktion.getTransaktionsLinjer().get(0), transaktionsLinje);
		assertEquals(transaktion.getTransaktionsLinjer().size(), 1);
		assertEquals(transaktion.getTransaktionsLinjer().get(0).getProdukt(), produkt);
		assertEquals(transaktion.getTransaktionsLinjer().get(0).getAntal(), 1);
	}
	
	@Test
	public void TC2() {
		TransaktionsLinje transaktionsLinje = null;
		
		assertEquals(transaktionsLinje, null);
		
		transaktionsLinje = transaktion.opretTransaktionsLinje(5, produkt);
		
		assertEquals(transaktion.getTransaktionsLinjer().get(0), transaktionsLinje);
		assertEquals(transaktion.getTransaktionsLinjer().size(), 1);
		assertEquals(transaktion.getTransaktionsLinjer().get(0).getProdukt(), produkt);
		assertEquals(transaktion.getTransaktionsLinjer().get(0).getAntal(), 5);
	}
	
	@Test
	public void TC3() {
		
		try {
			TransaktionsLinje transaktionsLinje = null;
			
			assertEquals(transaktionsLinje, null);
			
			transaktionsLinje = transaktion.opretTransaktionsLinje(0, produkt);
			fail();
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Pre condition violated");
		}
	}
	
	@Test
	public void TC4() {
		
		try {
			TransaktionsLinje transaktionsLinje = null;
			
			assertEquals(transaktionsLinje, null);
			
			transaktionsLinje = transaktion.opretTransaktionsLinje(-5, produkt);
			fail();
		} catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Pre condition violated");
		}
	}
	
	

}
