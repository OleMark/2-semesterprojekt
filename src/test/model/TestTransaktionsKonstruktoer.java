package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Transaktion;

public class TestTransaktionsKonstruktoer {

	//Test af konstruktoer.
	@Test
	public void TC1() {
		String medarbejder = "Ole";
		String ID = "123";
		
		Transaktion t = null;
		
		assertTrue(t == null);
		
		t = new Transaktion(medarbejder, ID);
		
		assertTrue(t != null);
		assertTrue(t instanceof Transaktion);
		assertEquals(t.getMedarbejder(), medarbejder);
		assertEquals(t.getID(), ID);
	}

}
