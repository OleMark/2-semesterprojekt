package test.model;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import application.model.Transaktion;

public class TestGetDatoGennemfoert {

	@Test
	public void TC1() {
		Transaktion t = new Transaktion("Ole", "123");
		
		t.setDatoGennemfoert();
		
		assertEquals(t.getDatoGennemfoert(), LocalDate.now());
	}

}
