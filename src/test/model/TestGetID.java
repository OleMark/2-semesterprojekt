package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Transaktion;

public class TestGetID {

	@Test
	public void TC1() {
		String ID = "123";
		
		Transaktion t = new Transaktion("Ole", ID);
		
		assertEquals(t.getID(), ID);
	}

}
