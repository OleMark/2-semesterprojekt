package test.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import application.controller.Controller;
import application.model.Prisliste;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Rabat;
import application.model.Transaktion;
import application.model.TransaktionsLinje;

public class TestSamletPris {

	private Transaktion transaktion;
	private Produktgruppe produktgruppe;
	private Produkt produkt;
	private Prisliste prisliste;

	@Before
	public void setUp() {
		transaktion = new Transaktion("Ole", "123");
		produktgruppe = new Produktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		produkt = new Produkt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
		prisliste = new Prisliste("Fredagsbar");
		prisliste.opretPrislisteLinje(36, produkt);

	}

	// Med transaktionlinje og rabat
	@Test
	public void TC1() {
		Rabat rabat = new Rabat(20);
		TransaktionsLinje transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
		Controller.setPrisPaaTransaktionsLinje(transaktionsLinje, prisliste, transaktion);
		transaktion.setRabat(rabat);

		assertEquals(16.0, transaktion.getSamletPris(), 0.01);

	}

	// Med transaktionlinje
	@Test
	public void TC2() {
		TransaktionsLinje transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
		Controller.setPrisPaaTransaktionsLinje(transaktionsLinje, prisliste, transaktion);

		transaktion.udregnPris();

		assertEquals(36.0, transaktion.getSamletPris(), 0.01);

	}

	// Med Rabat
	@Test
	public void TC3() {
		Rabat rabat = new Rabat(20);
		transaktion.setRabat(rabat);

		assertEquals(0.0, transaktion.getSamletPris(), 0.01);
	}

	// Uden noget
	@Test
	public void TC4() {

		transaktion.udregnPris();

		assertEquals(0.0, transaktion.getSamletPris(), 0.01);
	}

	// rabat > samletPris
	@Test
	public void TC5() {
		Rabat rabat = new Rabat(1000);
		TransaktionsLinje transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
		Controller.setPrisPaaTransaktionsLinje(transaktionsLinje, prisliste, transaktion);
		transaktion.setRabat(rabat);

		assertEquals(0.0, transaktion.getSamletPris(), 0.01);
	}

	// rabat > 0 && rabat < 1
	@Test
	public void TC6() {
		Rabat rabat = new Rabat(0.1);
		TransaktionsLinje transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
		Controller.setPrisPaaTransaktionsLinje(transaktionsLinje, prisliste, transaktion);
		transaktion.setRabat(rabat);


		assertEquals(32.4, transaktion.getSamletPris(), 0.01);
	}

}
