package test.model;

import static org.junit.Assert.*;

import org.junit.Test;

import application.model.Rabat;
import application.model.Transaktion;

public class TestSetRabatITransaktion {
	
	@Test
	public void TC1() {
		Transaktion transaktion = new Transaktion("Ole", "123");
		Rabat rabat = new Rabat(0.1);
		
		assertEquals(transaktion.getRabat(), null);
		
		transaktion.setRabat(rabat);
		
		assertEquals(transaktion.getRabat(), rabat);
	}
	
	@Test
	public void TC2() {
		Transaktion transaktion = new Transaktion("Ole", "123");
		Rabat rabat = new Rabat(0.1);
		Rabat rabat2 = new Rabat(0.2);
		
		transaktion.setRabat(rabat);
		
		try {
			transaktion.setRabat(rabat2);
			fail();
		}
		catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Pre condition violated");
		}
	}
}
