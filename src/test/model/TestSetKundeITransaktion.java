package test.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import application.model.Kunde;
import application.model.Transaktion;

public class TestSetKundeITransaktion {
	
	private Transaktion transaktion;
	private Kunde kunde;
	
	@Before
	public void setUp() {
		transaktion = new Transaktion("Ole", "123");
		kunde = new Kunde("Hans", "12345678", "hans123@hans.dk");
		
	}

	@Test
	public void TC1() {
		
		assertEquals(transaktion.getKunde(), null);

		transaktion.setKunde(kunde);
		
		assertEquals(transaktion.getKunde(), kunde);
		
		
	}

	
	@Test
	public void TC2() {
		
		transaktion.setKunde(kunde);
		
		Kunde kunde2 = new Kunde("Jens", "87654321", "jens123@jens.dk");
		
		try {
			transaktion.setKunde(kunde2);
			fail();
		}
		catch (RuntimeException e) {
			assertEquals(e.getMessage(), "Pre condition violated");
		}
		
	}
}
