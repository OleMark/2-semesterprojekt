package test.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Transaktion;
import application.model.TransaktionsLinje;

public class TestSletTransaktionsLinje {
	
	private Transaktion transaktion;
	private Produktgruppe produktgruppe;
	private Produkt produkt;
	private TransaktionsLinje transaktionsLinje;
	
	@Before
	public void setUp() {
		transaktion = new Transaktion("Ole", "123");
		produktgruppe = new Produktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		produkt = new Produkt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
		transaktionsLinje = transaktion.opretTransaktionsLinje(1, produkt);
	}

	@Test
	public void TC1() {
		assertEquals(transaktion.getTransaktionsLinjer().size(), 1);
		assertEquals(transaktion.getTransaktionsLinjer().get(0), transaktionsLinje);
		transaktion.fjernTransaktionsLinje(transaktionsLinje);
		assertEquals(transaktion.getTransaktionsLinjer().size(), 0);
	}
	
	@Test
	public void TC2() {
		assertEquals(transaktion.getTransaktionsLinjer().size(), 1);
		assertEquals(transaktion.getTransaktionsLinjer().get(0), transaktionsLinje);
		
		Produkt produkt2 = new Produkt("Imperial Stout", "Imperial Stout beskrivelse", produktgruppe);
		TransaktionsLinje transaktionsLinje2 = transaktion.opretTransaktionsLinje(2, produkt2);
		
		transaktion.fjernTransaktionsLinje(transaktionsLinje2);
		assertEquals(transaktion.getTransaktionsLinjer().size(), 1);
		assertEquals(transaktion.getTransaktionsLinjer().get(0), transaktionsLinje);
	}
	

}
