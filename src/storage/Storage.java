package storage;

import java.util.ArrayList;
import java.util.List;

import application.model.Kunde;
import application.model.Prisliste;
import application.model.Produktgruppe;
import application.model.Rabat;
import application.model.Transaktion;

/**
 * Denne klasse modellerer et storage. Storage indeholder alle de oprettede
 * objekter, som oprettes selvstaendigt (Altsaa findes produkter under
 * produktgrupper, og transaktionslinjer findes under transaktioner)
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public class Storage {
	// Liste over alle oprettede transaktioner
	private static List<Transaktion> transaktioner = new ArrayList<>();
	// Liste over alle oprettede produktgrupper
	private static List<Produktgruppe> produktgrupper = new ArrayList<>();
	// Liste over alle oprettede kunder
	private static List<Kunde> kunder = new ArrayList<>();
	// Liste over alle oprettede rabattter
	private static List<Rabat> rabatter = new ArrayList<>();
	// Liste over alle oprettede prislister
	private static List<Prisliste> prislister = new ArrayList<>();

	/**
	 * Returnerer en liste over alle oprettede transaktioner
	 * 
	 * @return liste over alle oprettede transaktioner
	 */

	public static List<Transaktion> getTransaktioner() {
		return new ArrayList<>(transaktioner);
	}

	/**
	 * Returnerer en liste over alle oprettede produktgrupper
	 * 
	 * @return liste over alle oprettede produktgrupper
	 */

	public static List<Produktgruppe> getProduktgrupper() {
		return new ArrayList<>(produktgrupper);
	}

	/**
	 * Returnerer en liste over alle oprettede kunder
	 * 
	 * @return liste over alle oprettede kunder
	 */

	public static List<Kunde> getKunder() {
		return new ArrayList<>(kunder);
	}

	/**
	 * Returnerer en liste over alle oprettede rabatter
	 * 
	 * @return liste over alle oprettede rabatter
	 */

	public static List<Rabat> getRabatter() {
		return new ArrayList<>(rabatter);
	}

	/**
	 * Returnerer en liste over alle oprettede prislistelinjer
	 * 
	 * @return liste over alle oprettede prislistelinjer
	 */

	public static List<Prisliste> getPrislister() {
		return new ArrayList<>(prislister);
	}

	/**
	 * Tilfoejer en transaktion til storage
	 * 
	 * @param transaktion som skal tilfoejes til storage
	 */

	public static void addTransaktion(Transaktion t) {
		if (!transaktioner.contains(t)) {
			transaktioner.add(t);
		}
	}

	/**
	 * Tilfoejer en produktgruppe til storage
	 * 
	 * @param produktgruppe som skal tilfoejes til storage
	 */

	public static void addProduktgruppe(Produktgruppe pg) {
		if (!produktgrupper.contains(pg)) {
			produktgrupper.add(pg);
		}
	}

	/**
	 * Tilfoejer en kunde til storage
	 * 
	 * @param kunde som skal tilfoejes til storage
	 */

	public static void addKunde(Kunde k) {
		if (!kunder.contains(k)) {
			kunder.add(k);
		}
	}

	/**
	 * Tilfoejer en rabat til storage
	 * 
	 * @param rabat som skal tilfoejes til storage
	 */

	public static void addRabat(Rabat r) {
		if (!rabatter.contains(r)) {
			rabatter.add(r);
		}
	}

	/**
	 * Tilfoejer en til prisliste storage
	 * 
	 * @param prisliste som skal tilfoejes til storage
	 */

	public static void addPrisliste(Prisliste pl) {
		if (!prislister.contains(pl)) {
			prislister.add(pl);
		}
	}

}
