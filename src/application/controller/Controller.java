package application.controller;

import java.util.List;

import application.model.Betalingsform;
import application.model.Kunde;
import application.model.Prisliste;
import application.model.PrislisteLinje;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Rabat;
import application.model.Transaktion;
import application.model.TransaktionsLinje;
import storage.Storage;

/**
 * En controllerklasse som styrer modelklasserne og bliver kaldt fra GUI.
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */
public class Controller {

	/**
	 * Opretter en transaktion med en medarbejder og et ID. Transaktionen bliver
	 * gemt i Storage.
	 * 
	 * @param medarbejder Medarbejderen der staar for transaktionen.
	 * @param ID          ID'et paa transaktionen.
	 * @return Transaktionen som er blevet oprettet.
	 */
	public static Transaktion opretTransaktion(String medarbejder, String ID) {
		Transaktion t = new Transaktion(medarbejder, ID);
		Storage.addTransaktion(t);
		return t;
	}

	/**
	 * Opretter en produktgruppe med et navn og en beskrivelse. Produktgruppen
	 * bliver gemt i Storage.
	 * 
	 * @param navn        Navnet paa produktgruppen.
	 * @param beskrivelse Beskrivelsen paa produktgruppen.
	 * @return Produktgruppen som er blevet oprettet.
	 */
	public static Produktgruppe opretProduktgruppe(String navn, String beskrivelse) {
		Produktgruppe pg = new Produktgruppe(navn, beskrivelse);
		Storage.addProduktgruppe(pg);
		return pg;
	}

	/**
	 * Opretter en kunde med et navn, et telefonnummer og en email. Kunden bliver
	 * gemt i Storage.
	 * 
	 * @param navn  Navnet paa kunden.
	 * @param tlfNr Telefonnummeret paa kunden.
	 * @param email Email for kunden
	 * @return Kunden som er blevet oprettet.
	 */
	public static Kunde opretKunde(String navn, String tlfNr, String email) {
		Kunde k = new Kunde(navn, tlfNr, email);
		Storage.addKunde(k);
		return k;
	}

	/**
	 * Opretter en Rabat. Denne bliver tilfoejet til Storage.
	 * 
	 * @param rabat Afhaengig af vaerdien er denne en procentrabat eller en
	 *              kontantrabat.
	 * @return Rabatten som er blevet oprettet.
	 */
	public static Rabat opretRabat(double rabat) {
		Rabat r = new Rabat(rabat);
		Storage.addRabat(r);
		return r;
	}

	/**
	 * Oprettet en Prisliste og tilfoejer den til Storage.
	 * 
	 * @param navn Navnet paa prislisten.
	 * @return Prislisten som er blevet oprettet.
	 */
	public static Prisliste opretPrisliste(String navn) {
		Prisliste pl = new Prisliste(navn);
		Storage.addPrisliste(pl);
		return pl;
	}

	/**
	 * Saetter prisen på en TransaktionsLinje.
	 * 
	 * @param transaktionsLinje TransaktionsLinjen hvor prisen skal saettes.
	 * @param pris              Prisen som TransaktionsLinjen skal saettes til.
	 */
	public static void setPrisPaaTransaktionsLinje(TransaktionsLinje transaktionsLinje, double pris, Transaktion transaktion) {
		transaktionsLinje.setPris(pris);
		opdaterPrisTransaktion(transaktion);
	}

	/**
	 * saetter prisen i TransaktionsLinjen til prisen i PrislisteLinjen.
	 * 
	 * @param transaktionsLinje Transaktionslinjen hvor prisen skal saettes.
	 * @param prisliste         Prislisten vi skal finde prisen for produktet til.
	 */
	public static void setPrisPaaTransaktionsLinje(TransaktionsLinje transaktionsLinje, Prisliste prisliste, Transaktion transaktion) {
		double pris = findPrisPaaProduktIPrisliste(prisliste, transaktionsLinje.getProdukt());
		setPrisPaaTransaktionsLinje(transaktionsLinje, pris, transaktion);
	}

	/**
	 * Denne metode finder prisen paa et produkt ud fra en prisliste. Vi går igennem
	 * alle prislisteLinjerne i et produkt og finder prisen der hoerer til
	 * prislisten.
	 * 
	 * @param prisliste Prislisten som vi skal finde den tilhoerende prislisteLinje
	 *                  til.
	 * @param produkt   Produktet som vi vil finde prisen paa.
	 * @return Prisen for produktet i den givne prisliste.
	 * @exception IllegalArgumentException Kastes hvis PrislisteLinjen for produktet
	 *                                     og Prislisten ikke findes.
	 */
	public static double findPrisPaaProduktIPrisliste(Prisliste prisliste, Produkt produkt) {
		int i = 0;
		double result = 0.0;
		boolean found = false;
		List<PrislisteLinje> prislisteLinjer = produkt.getPrislisteLinjer();

		while (!found && i < prislisteLinjer.size()) {
			if (prislisteLinjer.get(i).getPrisliste().equals(prisliste)) {
				result = prislisteLinjer.get(i).getPris();
				found = true;
			}
		}

		// Kast en exception hvis vi ikke finder prisen da produktet saa ikke har en
		// pris i den valgte prisliste.
		if (found == false) {
			throw new IllegalArgumentException("Der findes ingen pris til produktet for denne prisliste");
		}
		return result;
	}

	/**
	 * Opretter en TransaktionLinje tilhoerende en Transaktion med et antal af et
	 * produkt samt en prisliste hvor prisen paa produktet skal hentes.
	 * 
	 * @param transaktion Transaktionen som TransaktionsLinjen hoerer til.
	 * @param antal       Antallet af produktet paa TransaktionsLinjen.
	 * @param produkt     Produktet som hoerer til TransaktionLinjen.
	 * @param prisliste   Prislisten hvor prisen paa produktet skal findes.
	 * @return TransaktionLinjen som er blevet oprettet i transaktionen.
	 */
	public static TransaktionsLinje opretTransaktionsLinje(Transaktion transaktion, int antal, Produkt produkt,
			Prisliste prisliste) {
		TransaktionsLinje tl = transaktion.opretTransaktionsLinje(antal, produkt);

		/*
		 * Saetter prisen på transaktionslinjen efter at den er oprettet. Den skal
		 * saettes efter oprettelsen, da vi skal udregne den ud fra prislisten, som
		 * Transaktionslinjen ikke har adgang til.
		 */
		setPrisPaaTransaktionsLinje(tl, prisliste, transaktion);
		
		return tl;
	}

	/**
	 * Fjerner en transaktionslinje fra en transaktion
	 * 
	 * @param transaktionsLinje Transaktionslinjen der skal fjernes
	 * @param transaktion       Transaktionen som transaktionslinjen skal fjernes
	 *                          fra.
	 */
	public static void fjernTransaktionsLinjeFraTransaktion(TransaktionsLinje transaktionsLinje,
			Transaktion transaktion) {
		transaktion.fjernTransaktionsLinje(transaktionsLinje);
	}

	/**
	 * Fjerner en liste af transaktionsLinjer fra transaktionen
	 * @param transaktionsLinjeListe Listen af transaktionslinjer der skal fjernes
	 * @param transaktion Transaktionen som listen af transaktionslinjer skal fjernes fra.
	 */
	public static void fjernTransaktionsLinjeListeFraTransaktion(List<TransaktionsLinje> transaktionsLinjeListe,
			Transaktion transaktion) {
		for (TransaktionsLinje tl : transaktionsLinjeListe) {
			fjernTransaktionsLinjeFraTransaktion(tl, transaktion);
		}
	}

	/**
	 * Gennemfoer en transaktion med en betalingsform. Datoen for
	 * transaktionsgennemfoerrelsen saettes til dags dato.
	 * 
	 * @param transaktion   Transaktionen som skal gennemfoeres.
	 * @param betalingsform Betalingsformen som transaktionen gennemfoeres med.
	 */
	public static void gennemfoerTransaktion(Transaktion transaktion, Betalingsform betalingsform) {
		transaktion.setBetalingsform(betalingsform);
		transaktion.setDatoGennemfoert();
	}

	/**
	 * Tilfoej en Rabat til en Kunde.
	 * 
	 * @param kunde Kunden som skal have rabatten.
	 * @param rabat Rabatten som kunden skal have.
	 */
	public static void tilfoejRabatTilKunde(Kunde kunde, Rabat rabat) {
		kunde.setRabat(rabat);
	}

	/**
	 * Fjerner rabatten fra en kunde.
	 * 
	 * @param kunde Kunden som skal have fjernet rabatten.
	 */
	public static void fjernRabatFraKunde(Kunde kunde) {
		kunde.setRabatNull();
	}

	/**
	 * Tilfoejer en rabat til en transaktion.
	 * 
	 * @param transaktion Transaktion som skal have en rabat.
	 * @param rabat       Rabatten som skal tilfoejes transaktionen.
	 */
	public static void tilfoejRabatTilTransaktion(Transaktion transaktion, Rabat rabat) {
		transaktion.setRabat(rabat);
	}

	/**
	 * Fjerner rabatten fra transaktionen.
	 * 
	 * @param transaktion Transaktionen som rabatten skal fjernes fra.
	 */
	public static void fjernRabatFraTransaktion(Transaktion transaktion) {
		transaktion.setRabatNull();
	}

	/**
	 * Flytter et Produkt fra en produktgruppe til en anden.
	 * 
	 * @param produktgruppe Produktgruppen som produktet skal flyttes til.
	 * @param produkt       Produktet som skal flyttes.
	 */
	public static void setProduktgruppeForProdukt(Produktgruppe produktgruppe, Produkt produkt) {
		produkt.setProduktgruppeNull();
		produkt.setProduktgruppe(produktgruppe);
	}

	/**
	 * Sletter en PrislisteLinje fra en Prisliste. Dette vil samtidig fjerne
	 * produktet fra PrislisteLinjen, saa PrislisteLinjen ikke refereres fra noget
	 * andet objekt.
	 * 
	 * @param prislisteLinje Prislistelinjen som skal slettes.
	 */
	public static void sletPrislisteLinje(PrislisteLinje prislisteLinje) {
		prislisteLinje.setPrislisteNull();
		prislisteLinje.setProduktNull();
	}

	/**
	 * Opretter en PrislisteLinje for et produkt i en Prisliste.
	 * 
	 * @param pris      Prisen for produktet i den specifikke Prisliste.
	 * @param prisliste Prislisten som PrislisteLinjen tilhoerer.
	 * @param produkt   Produktet som PrislisteLinjen repraesenterer.
	 * @return PrislisteLinjen som er blevet oprettet.
	 */
	public static PrislisteLinje opretPrislisteLinje(double pris, Prisliste prisliste, Produkt produkt) {
		PrislisteLinje pll = prisliste.opretPrislisteLinje(pris, produkt);
		return pll;
	}

	/**
	 * Opretter et produkt i en produktgruppe.
	 * 
	 * @param navn          Navnet paa produktet.
	 * @param beskrivelse   Beskrivelsen af produktet.
	 * @param produktgruppe Produktgruppen som produktet tilhoerer.
	 * @return Produktet som er blevet oprettet.
	 */
	public static Produkt opretProdukt(String navn, String beskrivelse, Produktgruppe produktgruppe) {
		Produkt p = new Produkt(navn, beskrivelse, produktgruppe);
		return p;
	}

	/**
	 * Sletter et produkt fra en produktgruppe. Samtidig skal prislistelinjerne for
	 * produktet fjernes.
	 * 
	 * @param produkt produktet som skal slettes.
	 */
	public static void sletProdukt(Produkt produkt) {
		for (PrislisteLinje pl : produkt.getPrislisteLinjer()) {
			produkt.fjernPrislisteLinje(pl);
		}
		produkt.setProduktgruppeNull();
	}

	/**
	 * Tilfoejer en kunde til en transaktion.
	 * 
	 * @param transaktion Transaktionen som kunden skal tilfoejes til.
	 * @param kunde       Kunden som skal tilfoejes til transaktionen.
	 */
	public static void tilfoejKundeTilTransaktion(Transaktion transaktion, Kunde kunde) {
		transaktion.setKunde(kunde);
	}

	/**
	 * Fjerner en kunde fra en transaktion.
	 * 
	 * @param transaktion Transaktionen som skal have fjernet sin kunde.
	 */
	public static void fjernKundeFraTransaktion(Transaktion transaktion) {
		transaktion.setKundeNull();
	}
	
	/**
	 * Opdaterer prisen på transaktionen.
	 * @param transaktion Transaktionen hvor prisen skal opdateres.
	 */
	public static void opdaterPrisTransaktion(Transaktion transaktion) {
		transaktion.udregnPris();
	}
	
	/**
	 * Hent alle transaktioner fra Storage
	 * @return alle transaktioner fra Storage
	 */
	public static List<Transaktion> getTransaktioner() {
		return Storage.getTransaktioner();
	}
	
	/**
	 * Hent alle Produktgrupper fra Storage
	 * @return alle Produktgrupper fra Storage
	 */
	public static List<Produktgruppe> getProduktgrupper() {
		return Storage.getProduktgrupper();
	}
	
	/**
	 * Hent alle prislister fra Storage
	 * @return alle prislister fra Storage
	 */
	public static List<Prisliste> getPrislister() {
		return Storage.getPrislister();
	}
	
	/**
	 * Hent alle rabatter fra Storage
	 * @return alle rabatter fra Storage
	 */
	public static List<Rabat> getRabatter() {
		return Storage.getRabatter();
	}
	
	/**
	 * Hent alle kunder fra Storage
	 * @return alle kunder fra Storage
	 */
	public static List<Kunde> getKunder() {
		return Storage.getKunder();
	}

	public static void initContent() {
		Produktgruppe produktgruppe = opretProduktgruppe("Flaskeøl", "Flaskeøl på 0,6 L");
		Produkt produkt = opretProdukt("Klosterbryg", "Klosterbryg beskrivelse", produktgruppe);
		Prisliste prisliste = opretPrisliste("Fredagsbar");
		prisliste.opretPrislisteLinje(36, produkt);
		opretRabat(10.0);
		opretKunde("Hans", "12345678", "hans123@hans.dk");
	}

}
