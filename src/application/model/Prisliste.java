package application.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Denne klasse modellerer en prisliste.
 * En prisliste kan have en liste af prislisteLinjer som er tilknyttet til prislisten.
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */
public class Prisliste {

	
	//Navnet paa prislisten
	private String navn;
	//Listen med prislisteLinjer
	private List<PrislisteLinje> prislisteLinjer = new ArrayList<PrislisteLinje>();
	
	/**
	 * Konstruktoeren for en prisliste. 
	 * @param navn Navnet paa prislisten.
	 */
	public Prisliste(String navn) {
		this.navn = navn;
	}

	/**
	 * Returnerer navnet paa prislisten.
	 * @return Navnet paa prislisten.
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Returnerer en kopi af listen med prislisteLinjer som tilhører prislisten.
	 * @return En kopi af listen med prislisteLinjer.
	 */
	public List<PrislisteLinje> getPrislisteLinjer() {
		return new ArrayList<>(prislisteLinjer);
	}

	/**
	 * Opretter en prislisteLinje og kobler den til prislisten. 
	 * @param pris Prisen paa prislisteLinjen.
	 * @param produkt Det produkt der er koblet til prislisteLinjen.
	 * @return den prislisteLinje der er blevet oprettet.
	 */
	public PrislisteLinje opretPrislisteLinje(double pris, Produkt produkt) {
		PrislisteLinje pll = new PrislisteLinje(pris, this, produkt);
		prislisteLinjer.add(pll);
		return pll;
	}

	/**
	 * Fjerner en prislisteLinje fra prislisten og afkobler prislisten fra prislisteLinjen.
	 * Pre: prislisteLinjen skal eksistere og være koblet til prislisten. 
	 * @param prislisteLinje Den prislisteLinje der skal fjernes.
	 */
	public void sletPrislisteLinje(PrislisteLinje prislisteLinje) {
		if (prislisteLinjer.contains(prislisteLinje)) {
			prislisteLinjer.remove(prislisteLinje);
			prislisteLinje.setPrislisteNull();
		}
	}
	
	@Override
	public String toString() {
		return navn;
	}


}
