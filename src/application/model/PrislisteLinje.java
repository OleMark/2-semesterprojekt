package application.model;

/**
 * Dette er en en klasse der modellerer hen linje i en prisliste.
 * Denne linje er forbundet til et produkt og en prisliste
 * og er en pris i denne prisliste til dette specifikke produkt.
 * @author Oliver, Jeppe, Thor, Ole
 *
 */

public class PrislisteLinje {
	//Prisen paa produktet
	private double pris;
	//Produktet der hører til prislistelinjen
	private Produkt produkt;
	//Prislisten som linjen hoerer til.
	private Prisliste prisliste;

	/**
	 * Konstruktoer for Prislistelinje. Denne kaldes kun inde fra prisliste.
	 * Der skal vaere en pris, en prisliste som den skal hoere til 
	 * og et produkt som prisen tilhoerer.
	 * @param pris Prisen paa produktet.
	 * @param prisliste Prislisten som linjen tilhoerer.
	 * @param produkt Produktet som prisen tilhoerer.
	 */
	PrislisteLinje(double pris, Prisliste prisliste, Produkt produkt) {
		this.pris = pris;
		this.prisliste = prisliste;
		setProdukt(produkt);
	}

	/**
	 * Prisen paa produktet i den specifikke prisliste. 
	 * @return Prisen paa produktet.
	 */
	public double getPris() {
		return pris;
	}

	/**
	 * Returner produktet der hoerer til PrislisteLinjen.
	 * @return Produktet
	 */
	public Produkt getProdukt() {
		return produkt;
	}

	/**
	 * Returner listen denne linje hoerer til.
	 * @return Prislisten som denne linje er en del af.
	 */
	public Prisliste getPrisliste() {
		return prisliste;
	}
	
	/**
	 * Dette er lavet privat, da produktet skal saettes i konstruktoeren,
	 * og ikke skal kunne aendres senere.
	 * Her seattes produktet for linjen.
	 * @param produkt Produktet som hoerer til linjen
	 */
	private void setProdukt(Produkt produkt) {
		if (this.produkt != produkt) {
			this.produkt = produkt;
			produkt.tilfoejPrislisteLinje(this);
		}
	}
	
	/**
	 * Fjerner produktet fra linjen. Dette vil ogsaa fjerne linjen fra produktet.
	 * Dette fjerner linket mellem de to klasser helt. 
	 */
	public void setProduktNull() {
		if (this.produkt != null) {
			Produkt oldProdukt = this.produkt;
			this.produkt = null;
			oldProdukt.fjernPrislisteLinje(this);
			setPrislisteNull();
		}
	}
	
	/**
	 * Fjerner prislisten fra denne linje og denne linje fra prislisten.
	 * Dette gør at der ikke laengere er et link mellem de to klasser.
	 */
	public void setPrislisteNull() {
		if (this.prisliste != null) {
			Prisliste oldPrisliste = this.prisliste;
			this.prisliste = null;
			oldPrisliste.sletPrislisteLinje(this);
			setProduktNull();
		}
	}
	

}
