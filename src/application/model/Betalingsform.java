package application.model;

/**
 * Denne klasse modellerer en betalignsform. 
 * Hvis en transaktion ikke er blevet gennemfoert 
 * har den altid betalingsformen IKKEGENNEMFOERT
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public enum Betalingsform {
	KORT, KONTANT, KLIP, MOBILEPAY, IKKEGENNEMFOERT, ANNULLERET;
}
