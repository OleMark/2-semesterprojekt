package application.model;

/**
 * Denne klasse modellerer en transaktionslinje. 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public class TransaktionsLinje {

	// Antallet af et produkt på en transaktionslinje
	private int antal;
	// Prisen på en transaktionslinje
	private double pris;
	// Produktet på transaktionslinjen
	private Produkt produkt;

	/**
	 * Initialiserer en konstruktoer med et antal og et produkt.
	 * Denne konstruktoer er kun package synlig, og kan kun kaldes i Transaktion.
	 * Pre: antal >= 1
	 * @param antal
	 * @param produkt
	 */
	TransaktionsLinje(int antal, Produkt produkt) {
		Pre.require(antal >= 1);
		this.antal = antal;
		this.produkt = produkt;
	}

	/**
	 * Returnerer antallet til en transaktionslinje.
	 * @return antal.
	 */
	public int getAntal() {
		return antal;
	}

	/**
	 * Returnerer prisen.
	 * @return pris.
	 */
	public double getPris() {
		return pris;
	}
	
	/**
	 * Saetter prisen på en transaktionslinje.
	 * Pre: pris >= 0.0
	 * @param pris
	 */
	public void setPris(double pris) {
		Pre.require(pris > 0.0);
		this.pris = pris;
	}

	/**
	 * Returnerer et produkt til transaktionslinjen.
	 * @return produkt
	 */
	public Produkt getProdukt() {
		return produkt;
	}

	@Override
	public String toString() {
		return antal + " " + produkt + ", pris pr. stk: " + pris + ", pris i alt: " + antal*pris;
	}

}
