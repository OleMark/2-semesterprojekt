package application.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Denne klasse modellerer en produktgruppe.
 * En prisliste kan have en liste af produkter som er tilknyttet til gruppen.
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */
public class Produktgruppe {

	

	//Navnet paa produktgruppen.
	private String navn;
	//Beskrivelsen for produktgruppen.
	private String beskrivelse;
	//Listen af gruppens produkter.
	private List<Produkt> produkter = new ArrayList<Produkt>();

	/**
	 * Konstruktoeren for en produktgruppe. 
	 * @param navn Navnet paa produktgruppen.
	 * @param beskrivelse Beskrivelsen for produktgruppen.
	 */
	public Produktgruppe(String navn, String beskrivelse) {
		this.navn = navn;
		this.beskrivelse = beskrivelse;
	}

	/**
	 * Returnerer navnet paa produktgruppen.
	 * @return Navnet paa produktgruppen.
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Returnerer beskrivelsen for produktgruppen.
	 * @return Beskrivelsen for produktgruppen.
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Tilfoejer et produkt til produktgruppen, produktgruppen tilfoejs også til produktet.
	 * @param produkt Produktet som skal tilfoejes til produktgruppen.
	 */
	public void tilfoejProdukt(Produkt produkt) {
		if (!produkter.contains(produkt)) {
			produkter.add(produkt);
			produkt.setProduktgruppe(this);
		}
	}

	/**
	 * Fjerner et produkt fra produktgruppen, produktgruppen fjernes samtidig fra produktet.
	 * Pre: produktet eksisterer og der tilhører produktgruppen.
	 * @param produkt Produktet som skal fjernes fra produktgruppen.
	 */
	public void fjernProdukt(Produkt produkt) {
		if (produkter.contains(produkt)) {
			produkter.remove(produkt);
			produkt.setProduktgruppeNull();
		}
	}

	/**
	 * Returnerer en kopi af listen med produkter der er koblet på produktgruppen.
	 * @return En kopi af listen med produkter.
	 */
	public List<Produkt> getProdukter() {
		return new ArrayList<Produkt>(produkter);
	}
	
	@Override
	public String toString() {
		return navn;
	}

}
