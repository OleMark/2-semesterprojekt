package application.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Denne klasse modellerer en transaktion. Transakionen bestaar af en eller
 * flere transaktionslinjer, (hvor information om transaktionens produkter,
 * priser og antal findes) En transaktion kan have en kunde og/eller en rabat
 * tilfoejet, hvor samletPris evt. bliver justeret efter dette.
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public class Transaktion {
	// Datoen transaktionen er oprettet
	private LocalDate datoOprettet;
	// Den samlede pris paa transaktionen
	private double samletPris;
	// Medarbejderen som har oprettet transaktionen
	private String medarbejder;
	// Den registrerede betalingsform (Bliver sat til IKKEGENNEMFOERT indtil
	// gennemforelsen af transaktionen)
	private Betalingsform betalingsform;
	// Datoen for gennemfoerelse af transaktionen
	private LocalDate datoGennemfoert;
	// Unikt ID for transaktionen
	private String ID;
	// Listen over de oprettede transaktionslinjer til transaktionen
	private List<TransaktionsLinje> transaktionsLinjer = new ArrayList<>();
	// Den evt. tilfoejede rabat. Nullable.
	private Rabat rabat;
	// Den evt. tilfoejede kunde. Nullable.
	private Kunde kunde;

	/**
	 * Konstruktoeren for en Transaktion datoOprettet bliver altid sat til dags dato
	 * 
	 * @param medarbejder
	 * @param ID
	 */

	public Transaktion(String medarbejder, String ID) {
		datoOprettet = LocalDate.now();
		this.medarbejder = medarbejder;
		this.ID = ID;
		betalingsform = Betalingsform.IKKEGENNEMFOERT;
	}

	/**
	 * Returnerer datoen for oprettelse af transaktionen
	 * 
	 * @return datoen for oprettelse af transaktionen
	 */

	public LocalDate getDatoOprettet() {
		return datoOprettet;
	}

	/**
	 * Returnerer den samlede pris for transaktionen
	 * 
	 * @return samlet pris for transaktionen
	 */

	public double getSamletPris() {
		return samletPris;
	}

	/**
	 * Metode til test af samletPris NOTE: KUN TIL TEST!
	 * 
	 * @param pris Prisen der skal saettes
	 */
	public void setPrisTest(double pris) {
		samletPris = pris;
	}

	/**
	 * Returnerer den registrerede medarbejder for den oprettede transaktion
	 * Registrerede medarbejder for den oprettede transaktion
	 * 
	 * @return
	 */

	public String getMedarbejder() {
		return medarbejder;
	}

	/**
	 * Returnerer betalingsformen for transaktionen
	 * 
	 * @return betalingsformen for transaktionen
	 */

	public Betalingsform getBetalingsform() {
		return betalingsform;
	}

	/**
	 * Returnerer datoen for gennemfoerelsen af transaktionen
	 * 
	 * @return datoen for gennemfoerelsen af transaktionen
	 */

	public LocalDate getDatoGennemfoert() {
		return datoGennemfoert;
	}

	/**
	 * Saetter datoen for gennemforelsen af transaktionen
	 */

	public void setDatoGennemfoert() {
		datoGennemfoert = LocalDate.now();
	}

	/**
	 * Returnerer ID'et for transaktionen
	 * 
	 * @return ID'et for transaktionen
	 */

	public String getID() {
		return ID;
	}

	/**
	 * Returnerer en liste af de oprettede transaktionslinjer til transaktionen
	 * 
	 * @return liste af de oprettede transaktionslinjer til transaktionen
	 */

	public List<TransaktionsLinje> getTransaktionsLinjer() {
		return new ArrayList<>(transaktionsLinjer);
	}

	/**
	 * Returnerer den nuvaerende rabat paa transaktionen
	 * 
	 * @return den nuvaerende rabat paa transaktionen
	 */

	public Rabat getRabat() {
		return rabat;
	}

	/**
	 * Returnerer den eventuelt tilfoejede kunde for transaktionen
	 * 
	 * @return den eventuelt tilfoejede kunde for transaktionen
	 */

	public Kunde getKunde() {
		return kunde;
	}

	/**
	 * Opretter, tilfoejer og returnerer en ny transaktionslinje med et antal og
	 * produkt
	 * 
	 * @param antal
	 * @param produkt
	 * @return ny transaktionslinje som er tilfoejet til transaktionen
	 */

	public TransaktionsLinje opretTransaktionsLinje(int antal, Produkt produkt) {
		TransaktionsLinje tl = new TransaktionsLinje(antal, produkt);
		transaktionsLinjer.add(tl);
		udregnPris();
		return tl;
	}

	/**
	 * Fjerner en transaktionslinje fra transaktionen
	 * 
	 * @param transaktionsLinje der skal fjernes
	 */

	public void fjernTransaktionsLinje(TransaktionsLinje transaktionsLinje) {
		if (transaktionsLinjer.contains(transaktionsLinje)) {
			transaktionsLinjer.remove(transaktionsLinje);
			udregnPris();
		}
	}

	/**
	 * Saetter rabatten for transaktionen Pre: this.rabat == null 
	 * 
	 * @param rabat
	 */
	public void setRabat(Rabat rabat) {
		if (this.rabat != rabat) {
			Pre.require(this.rabat == null);
			this.rabat = rabat;
			udregnPris();
		}
	}

	/**
	 * Saetter rabatten til null
	 */

	public void setRabatNull() {
		if (rabat != null) {
			rabat = null;
			udregnPris();
		}
	}

	/**
	 * Saetter kunden for transaktionen og saetter rabatten, hvis kunden har en
	 * rabat med sig Pre: this.kunde == null
	 * 
	 * @param kunde som skal tilfoejes til transaktionen
	 */
	public void setKunde(Kunde kunde) {
		if (this.kunde != kunde) {
			Pre.require(this.kunde == null);
			this.kunde = kunde;
			kunde.tilfoejTransaktion(this);
			if (kunde.getRabat() != null) {
				setRabat(kunde.getRabat());
			}
		}
	}

	/**
	 * Saetter kunden til null
	 */

	public void setKundeNull() {
		if (kunde != null) {
			Kunde oldKunde = kunde;
			kunde = null;
			oldKunde.fjernTransaktion(this);
			if (oldKunde.getRabat() == rabat) {
				setRabatNull();
			}
		}
	}

	/**
	 * Saetter betalingsformen for transaktionen Pre: betalingsform ==
	 * IKKEGENNEMFOERT
	 * 
	 * @param betalingsform som skal tilfoejes til transaktionen
	 */
	public void setBetalingsform(Betalingsform betalingsform) {
		if (betalingsform != null) {
			Pre.require(this.betalingsform.equals(Betalingsform.IKKEGENNEMFOERT));
			this.betalingsform = betalingsform;
		}
	}

	/**
	 * Udregner og saetter samletpris for transaktionen
	 */

	public void udregnPris() {
		double totalPris = 0.0;

		for (TransaktionsLinje tl : transaktionsLinjer) {
			totalPris += tl.getPris() * tl.getAntal();
		}

		if (rabat != null) {
			totalPris = rabat.udregnSamletPrisMedRabat(totalPris);
		}

		samletPris = totalPris;
	}

}
