package application.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Denne klasse modellerer en Produkt.
 * Et produkt har en prislistelinje, hvor et produkt er tilknyttet.
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public class Produkt {


	//Navn på produktet
	private String navn;
	//Beskrivelse af produktet
	private String beskrivelse;
	//Linkattribut til Produktgruppe
	private Produktgruppe produktgruppe;
	//Linkattribut til PrislisteLinje
	private List<PrislisteLinje> prislisteLinjer = new ArrayList<>();
	
	/**
	 * Initialiserer et produkt med et navn, beskrivelse og en tilhørende produktgruppe.
	 * @param navn
	 * @param beskrivelse
	 * @param produktgruppe
	 */
	public Produkt(String navn, String beskrivelse, Produktgruppe produktgruppe) {
		this.navn = navn;
		this.beskrivelse = beskrivelse;
		setProduktgruppe(produktgruppe);
	}

	/**
	 * Returnerer navnet på produktet.
	 * @return navn.
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Returnerer beskrivelsen på produktet.
	 * @return beskrivelse.
	 */
	public String getBeskrivelse() {
		return beskrivelse;
	}

	/**
	 * Returnerer produktgruppen.
	 * @return produktgruppe.
	 */
	public Produktgruppe getProduktgruppe() {
		return produktgruppe;
	}
	
	/**
	 * Saetter produktgruppen til null. Skal bruges hvis produktet skal slettes 
	 * eller flyttes til en anden produktgruppe
	 */
	public void setProduktgruppeNull() {
		if (produktgruppe != null) {
			Produktgruppe oldProduktgruppe = produktgruppe;
			this.produktgruppe = null;
			oldProduktgruppe.fjernProdukt(this);
		}
	}
	
	/**
	 * Saetter produktgruppen til en ny produkgruppe 
	 * @param produktgruppe Den nye produktgruppe for produktet
	 * Pre: produktgruppen skal vaere null foer den kan saettes.
	 */
	public void setProduktgruppe(Produktgruppe produktgruppe) {
		if (this.produktgruppe != produktgruppe) {
			Pre.require(this.produktgruppe == null);
			this.produktgruppe = produktgruppe;
			produktgruppe.tilfoejProdukt(this);
		}
	}

	/**
	 * Returnerer en kopi af en ny priselistelinje.
	 * @return
	 */
	public List<PrislisteLinje> getPrislisteLinjer() {
		return new ArrayList<>(prislisteLinjer);
	}
	
	/**
	 * Vi har valgt at lave setProdukt til private i prislitelinjen, 
	 * da man ikke skal kunne skifte prislistelinjen fra et produkt til et andet
	 * @param prislisteLinje den p´PrislisteLinje man gerne vil tilfoeje!
	 * Pre: PrislisteLinje.getProdukt == this
	 */
	public void tilfoejPrislisteLinje(PrislisteLinje prislisteLinje) {
		if (!prislisteLinjer.contains(prislisteLinje)) {
			Pre.require(prislisteLinje.getProdukt() == this);
			prislisteLinjer.add(prislisteLinje);
		}
	}
	
	/**
	 * Fjerner en prislistelinje fra prislistelinjer, hvis den ikke allerede er
	 * i arraylisten. Derefter bliver produktet saettet til null.
	 * @param prislisteLinje
	 */
	public void fjernPrislisteLinje(PrislisteLinje prislisteLinje) {
		if (prislisteLinjer.contains(prislisteLinje)) {
			prislisteLinjer.remove(prislisteLinje);
			prislisteLinje.setProduktNull();
		}
	}
	
	@Override
	public String toString() {
		return navn;
	}
	
}
