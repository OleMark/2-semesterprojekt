package application.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Denne klasse modellerer en kunde.
 * En Kunde kan have en liste af transaktioner hvor kunden har vaeret tilknyttet.
 * Kunden kan ogsaa have en rabat tilføjet,
 * som automatisk bliver sat i en transaktion hvor kunden bliver tilknyttet.
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public class Kunde {

	//Navnet paa kunden
	private String navn;
	//Telefonnummeret paa kunden
	private String tlfNr;
	//Emailen paa kunden
	private String email;
	//Hvis kunden har en Rabat tilfoejet. Nullable
	private Rabat rabat;

	// Listen af transaktioner som kunden har vaeret en del af.
	private List<Transaktion> transaktioner = new ArrayList<Transaktion>();

	/**
	 * Konstruktoeren for en kunde. 
	 * @param navn Navnet paa kunden.
	 * @param tlfNr Telefonnummer paa kunden.
	 * @param email Email for kunden.
	 */
	public Kunde(String navn, String tlfNr, String email) {
		this.navn = navn;
		this.tlfNr = tlfNr;
		this.email = email;
	}

	/**
	 * Returnerer navnet paa kunden.
	 * @return Navnet paa kunden.
	 */
	public String getNavn() {
		return navn;
	}
	
	/**
	 * Returnerer telefonnummeret paa kunden.
	 * @return Telefonnummeret paa kunden.
	 */
	public String getTlfNr() {
		return tlfNr;
	}

	/**
	 * Returnerer emailen for kunden.
	 * @return Emailen for kunden.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Returnerer en kopi af listen af transaktioner som hoerer til kunden.
	 * @return En kopi af listen af transaktioner.
	 */
	public List<Transaktion> getTransaktioner() {
		return new ArrayList<>(transaktioner);
	}

	/**
	 * Returnerer rabatten som er knyttet til kunden, hvis denne eksisterer.
	 * Ellers returnerer den null.
	 * Note: Kan returnere null.
	 * @return Rabatten der er knyttet til kunden
	 */
	public Rabat getRabat() {
		return rabat;
	}

	/**
	 * Saet rabatten for kunden. Denne skal vaere null foer den kan saettes.
	 * Pre: this.rabat = null;
	 * @param rabat Rabatten som kunden skal have tilknyttet
	 */
	public void setRabat(Rabat rabat) {
		if (this.rabat != rabat) {
			Pre.require(this.rabat == null);
			this.rabat = rabat;
		}
	}

	/**
	 * En metode til at fjerne rabatten fra kunden.
	 */
	public void setRabatNull() {
		if (this.rabat != null) {
			this.rabat = null;
		}

	}

	/**
	 * Tilfoej en transaktion til kunden. Kunden tilfoejes samtidig til transaktionen.
	 * @param transaktion Transaktionen som skal tilfoejes til kunden.
	 */
	public void tilfoejTransaktion(Transaktion transaktion) {
		if (!transaktioner.contains(transaktion)) {
			transaktioner.add(transaktion);
			transaktion.setKunde(this);
		}
	}

	/**
	 * Fjern en transaktion fra kunden. Dette fjerner også kunden fra transaktionen.
	 * @param transaktion Transaktionen der skal fjernes.
	 */
	public void fjernTransaktion(Transaktion transaktion) {
		if (transaktion.getBetalingsform().equals(Betalingsform.IKKEGENNEMFOERT) && transaktioner.contains(transaktion)) {
			transaktioner.remove(transaktion);
			transaktion.setKundeNull();
		}
	}

}
