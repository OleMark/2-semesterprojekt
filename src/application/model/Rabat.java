package application.model;

/**
 * Denne klasse modellerer en rabat.
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 *
 */

public class Rabat {

	// Rabatten på prisen.
	// Hvis rabatten er mindre end 1.0, er det en procentvis rabat.
	// Ellers er det en kontant rabat.
	private double rabat;

	/**
	 * Initialiserer en constructor med en rabat. Rabatten skal vaere stoerre end 0,
	 * foerend constructoren bliver initialiseret. Pre: rabat > 0
	 * 
	 * @param rabat
	 */
	public Rabat(double rabat) {
		Pre.require(rabat > 0);
		this.rabat = rabat;
	}

	/**
	 * Returnerer rabatten.
	 * 
	 * @return rabatten.
	 */
	public double getRabat() {
		return rabat;
	}

	/**
	 * Udregner en samlet pris med en rabat. Hvis rabatten er mindre end 1.0, er det
	 * en procentvis rabat. Ellers er det en kontant rabat.
	 * 
	 * @param samletPris
	 * @return en samletPris med en rabat.
	 */
	public double udregnSamletPrisMedRabat(double samletPris) {
		double samletPrisMedrabat = (rabat < 1.0) ? samletPris * (1.0 - rabat) : samletPris - rabat;

		if (samletPrisMedrabat < 0.0) {
			return 0.0;
		} else {
			return samletPrisMedrabat;
		}
	}

}
