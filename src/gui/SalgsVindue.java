package gui;

import application.controller.Controller;
import application.model.Betalingsform;
import application.model.Prisliste;
import application.model.Transaktion;
import application.model.TransaktionsLinje;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Et salgvindue hvor man opretter en transaktion med produkter
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */

public class SalgsVindue extends Stage {

	private String medarbejder;
	private Prisliste prisliste;
	private Transaktion transaktion;

	public SalgsVindue(String title, String medarbejder, Prisliste prisliste) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		this.medarbejder = medarbejder;
		this.prisliste = prisliste;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	// -------------------------------------------------------------------------

	// Label med den samlede pris paa transaktionen
	private Label lblSamletPrisVaerdi;
	// Listen over de tilfoejede transaktionslinjer til transaktion
	private ListView<TransaktionsLinje> lvwTransaktionsLinjer;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(20);
		pane.setGridLinesVisible(false);

		Label lblMedarbejder = new Label("Medarbejder: " + medarbejder);
		pane.add(lblMedarbejder, 0, 0);

		Label lblTilfoejedeProdukter = new Label("Tilføjede produkter:");
		pane.add(lblTilfoejedeProdukter, 0, 1);
		GridPane.setValignment(lblTilfoejedeProdukter, VPos.BOTTOM);

		lvwTransaktionsLinjer = new ListView<>();
		pane.add(lvwTransaktionsLinjer, 0, 2, 3, 7);
		lvwTransaktionsLinjer.setPrefHeight(300);

		Button btnTilfoej = new Button("Tilføj");
		pane.add(btnTilfoej, 3, 2);
		btnTilfoej.setPrefSize(75, 40);
		btnTilfoej.setOnAction(e -> tilfoejProduktAction());

		Button btnFjern = new Button("Fjern");
		pane.add(btnFjern, 3, 8);
		btnFjern.setPrefSize(75, 40);
		GridPane.setValignment(btnFjern, VPos.TOP);
		btnFjern.setOnAction(e -> fjernProduktAction());

		Label lblRabat = new Label("Rabat: N/A");
		pane.add(lblRabat, 4, 1);

		Button btnRedigerRabat = new Button("Rediger\nrabat");
		pane.add(btnRedigerRabat, 4, 2);
		btnRedigerRabat.setPrefSize(75, 40);
		btnRedigerRabat.setTextAlignment(TextAlignment.CENTER);

		Button btnFjernRabat = new Button("Fjern\nrabat");
		pane.add(btnFjernRabat, 4, 3);
		btnFjernRabat.setPrefSize(75, 40);
		btnFjernRabat.setTextAlignment(TextAlignment.CENTER);

		Label lblKunde = new Label("Kunde: N/A");
		pane.add(lblKunde, 4, 6);

		Button btnRedigerKunde = new Button("Rediger\nkunde");
		pane.add(btnRedigerKunde, 4, 7);
		btnRedigerKunde.setPrefSize(75, 40);
		btnRedigerKunde.setTextAlignment(TextAlignment.CENTER);

		Button btnFjernKunde = new Button("Fjern\nkunde");
		pane.add(btnFjernKunde, 4, 8);
		btnFjernKunde.setPrefSize(75, 40);
		GridPane.setValignment(btnFjernKunde, VPos.TOP);
		btnRedigerKunde.setTextAlignment(TextAlignment.CENTER);

		Label lblPrisliste = new Label(prisliste.getNavn());
		pane.add(lblPrisliste, 5, 0, 2, 1);
		lblPrisliste.setStyle("-fx-font: 20 arial;");
		GridPane.setHalignment(lblPrisliste, HPos.CENTER);

		Label lblSamletPris = new Label("Samlet pris:");
		pane.add(lblSamletPris, 5, 6, 2, 1);
		lblSamletPris.setStyle("-fx-font: 16 arial;");
		GridPane.setHalignment(lblSamletPris, HPos.CENTER);

		lblSamletPrisVaerdi = new Label("0 kr");
		pane.add(lblSamletPrisVaerdi, 5, 7, 2, 1);
		lblSamletPrisVaerdi
				.setStyle("-fx-border-color:black; -fx-padding:3px; -fx-font: 22 arial; -fx-background-color:white;");
		GridPane.setHalignment(lblSamletPrisVaerdi, HPos.CENTER);
		lblSamletPrisVaerdi.setPrefWidth(110);
		lblSamletPrisVaerdi.setAlignment(Pos.BASELINE_RIGHT);

		Button btnBetal = new Button("Betal");
		pane.add(btnBetal, 6, 8);
		btnBetal.setPrefSize(75, 40);
		btnBetal.setOnAction(e -> betalAction());

		Button btnAnnullerSalg = new Button("Annuller\nsalg");
		pane.add(btnAnnullerSalg, 5, 8);
		btnAnnullerSalg.setPrefSize(75, 40);
		btnAnnullerSalg.setTextAlignment(TextAlignment.CENTER);
		btnAnnullerSalg.setOnAction(e -> AnnullerAction());

	}

	/**
	 * Opretter et objekt af klassen BetalingsVindue og viser denne, indtil
	 * gennemfoerelse af transaktionen
	 */

	private void betalAction() {
		if (transaktion == null) {
			return;
		}
		BetalingsVindue bv = new BetalingsVindue("Betal", transaktion);
		bv.showAndWait();
		if (transaktion.getBetalingsform() != Betalingsform.IKKEGENNEMFOERT) {
			close();
		}
	}

	/**
	 * Opretter et objekt af klassen TilfoejProduktTilTransaktionVindue og viser
	 * denne indtil produkter er valg Opdaterer listViewet over tilfoejede
	 * transaktionslinjer
	 */

	private void tilfoejProduktAction() {
		if (transaktion == null) {
			transaktion = Controller.opretTransaktion(medarbejder, Controller.getTransaktioner().size() + 1 + "");
		}

		TilfoejProduktTilTransaktionVindue tpttv = new TilfoejProduktTilTransaktionVindue("Tilføj produkt", transaktion,
				prisliste);
		tpttv.showAndWait();

		opdaterTransaktionsLinjeListView();
	}

	/**
	 * Fjerner en transaktionslinje fra den oprettede transaktion og opdaterer
	 * listViewet over tilfoejede transaktionslinjer
	 */

	private void fjernProduktAction() {
		TransaktionsLinje transaktionlinje = lvwTransaktionsLinjer.getSelectionModel().getSelectedItem();
		if (transaktionlinje != null) {
			Controller.fjernTransaktionsLinjeFraTransaktion(transaktionlinje, transaktion);
		}
		opdaterTransaktionsLinjeListView();
	}

	/**
	 * Opdaterer listViewet over tilfoejede transaktionslinjer til transkationen
	 */

	private void opdaterTransaktionsLinjeListView() {
		lvwTransaktionsLinjer.getItems().setAll(transaktion.getTransaktionsLinjer());
		opdaterPris();
	}

	/**
	 * Opdaterer den samlede pris for transaktionen
	 */

	private void opdaterPris() {
		if (transaktion != null) {
			lblSamletPrisVaerdi.setText(transaktion.getSamletPris() + " kr");
		}

	}

	/**
	 * Annullerer et salg jf. ordbog, og saetter betalingsformen for gennemfoert
	 * transaktion til Annulleret
	 */

	private void AnnullerAction() {
		if (transaktion != null) {
			Controller.gennemfoerTransaktion(transaktion, Betalingsform.ANNULLERET);
		}
		close();
	}

}
