package gui;

import application.controller.Controller;
import application.model.Betalingsform;
import application.model.Transaktion;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Et betalingsvindue hvor man kan gennemfoere en transaktion
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */

public class BetalingsVindue extends Stage {

	// Transaktion der er blevet oprettet i salgsvinduet
	private Transaktion transaktion;

	public BetalingsVindue(String title, Transaktion transaktion) {

		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		this.transaktion = transaktion;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setGridLinesVisible(false);

		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		Button btnKontant = new Button("Kontant");
		pane.add(btnKontant, 1, 4);
		GridPane.setMargin(btnKontant, new Insets(10, 10, 10, 10));
		btnKontant.setPrefSize(100, 50);
		btnKontant.setOnAction(event -> kontantAction());

		Button btnKlip = new Button("Klip");
		pane.add(btnKlip, 3, 4);
		GridPane.setMargin(btnKlip, new Insets(10, 10, 10, 10));
		btnKlip.setPrefSize(100, 50);
		btnKlip.setOnAction(event -> this.klipAction());

		Button btnKort = new Button("Kort");
		pane.add(btnKort, 1, 6);
		GridPane.setMargin(btnKort, new Insets(10, 10, 10, 10));
		btnKort.setPrefSize(100, 50);
		btnKort.setOnAction(event -> this.kortAction());

		Button btnMobilePay = new Button("MobilePay");
		pane.add(btnMobilePay, 3, 6);
		GridPane.setMargin(btnMobilePay, new Insets(10, 10, 10, 10));
		btnMobilePay.setPrefSize(100, 50);
		btnMobilePay.setOnAction(event -> this.mobilePayAction());

		Button btnAnnuller = new Button("Annuller");
		pane.add(btnAnnuller, 3, 7);
		GridPane.setMargin(btnAnnuller, new Insets(10, 10, 10, 10));
		GridPane.setHalignment(btnAnnuller, HPos.RIGHT);
		btnAnnuller.setOnAction(event -> this.annullerAction());

	}

	/**
	 * Gennemfoerer en transaktion og saetter betalingsformen til kontant
	 */

	private void kontantAction() {
		Controller.gennemfoerTransaktion(transaktion, Betalingsform.KONTANT);
		close();
	}

	/**
	 * Gennemfoerer en transaktion og saetter betalingsformen til klip
	 */

	private void klipAction() {
		Controller.gennemfoerTransaktion(transaktion, Betalingsform.KLIP);
		close();
	}

	/**
	 * Gennemfoerer en transaktion og saetter betalingsformen til MobilePay
	 */

	private void mobilePayAction() {
		Controller.gennemfoerTransaktion(transaktion, Betalingsform.MOBILEPAY);
		close();
	}

	/**
	 * Gennemfoerer en transaktion og saetter betalingsformen til kort
	 */

	private void kortAction() {
		Controller.gennemfoerTransaktion(transaktion, Betalingsform.KORT);
		close();
	}

	/**
	 * Lukker betalingsvinduet uden at gennemfoere transkaktionen
	 */

	private void annullerAction() {
		close();
	}
}
