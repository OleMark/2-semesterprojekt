package gui;

import application.controller.Controller;
import application.model.Prisliste;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Dette er hovedvinduet til vores GUI. Her findes der knapper til salg, retur,
 * udlejning og statistik. I version 1.0 er kun salgsdelen af systemet aktivt.
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() {
		Controller.initContent();
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Kasseapparat");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// ------------------------------------------------------------------------

	private TextField txfMedarbejder;
	private ComboBox<Prisliste> cbxPrisliste;

	private void initContent(GridPane pane) {
		pane.setGridLinesVisible(false);

		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblMedarbejder = new Label("Medarbejder:");
		pane.add(lblMedarbejder, 1, 0);
		GridPane.setMargin(lblMedarbejder, new Insets(10, 10, 0, 10));

		txfMedarbejder = new TextField();
		pane.add(txfMedarbejder, 1, 1, 3, 1);
		txfMedarbejder.setPrefWidth(240);
		GridPane.setMargin(txfMedarbejder, new Insets(0, 10, 10, 10));
		txfMedarbejder.setText("Ole");

		Label lblPrisliste = new Label("Prisliste:");
		pane.add(lblPrisliste, 1, 2);
		GridPane.setMargin(lblPrisliste, new Insets(0, 10, 0, 10));

		// Combobox med prislister
		cbxPrisliste = new ComboBox<Prisliste>();
		cbxPrisliste.getItems().addAll(Controller.getPrislister());
		pane.add(cbxPrisliste, 1, 3, 3, 1);
		cbxPrisliste.getSelectionModel().select(0);
		cbxPrisliste.setPrefWidth(240);
		GridPane.setMargin(cbxPrisliste, new Insets(0, 10, 10, 10));
		ChangeListener<Prisliste> listener = (ov, oldListe, newListe) -> this.selectedListChanged();
		cbxPrisliste.getSelectionModel().selectedItemProperty().addListener(listener);

		Button btnSalg = new Button("Salg");
		pane.add(btnSalg, 1, 4);
		GridPane.setMargin(btnSalg, new Insets(10, 10, 10, 10));
		btnSalg.setPrefSize(100, 50);
		btnSalg.setOnAction(event -> salgAction());

		Button btnRetur = new Button("Retur");
		pane.add(btnRetur, 3, 4);
		GridPane.setMargin(btnRetur, new Insets(10, 10, 10, 10));
		btnRetur.setPrefSize(100, 50);
		btnRetur.setOnAction(event -> this.orgAction());

		Button btnUdlejning = new Button("Udlejning");
		pane.add(btnUdlejning, 1, 6);
		GridPane.setMargin(btnUdlejning, new Insets(10, 10, 10, 10));
		btnUdlejning.setPrefSize(100, 50);
		btnUdlejning.setOnAction(event -> this.orgAction());

		Button btnStatistik = new Button("Statistik");
		pane.add(btnStatistik, 3, 6);
		GridPane.setMargin(btnStatistik, new Insets(10, 10, 10, 10));
		btnStatistik.setPrefSize(100, 50);
		btnStatistik.setOnAction(event -> this.orgAction());

	}

	/**
	 * Opdaterer comboboxen så alle prislister er med. bliver ikke rigtigt kaldt i
	 * version 1.0, men skal bruges i fremtidige version
	 */
	private void selectedListChanged() {
		cbxPrisliste.getItems().setAll(Controller.getPrislister());
	}

	/**
	 * Opret et salgsvindue hvor man kan saelge varer og gennemfoere en transaktion.
	 * Giver en fejl hvis medarbejderfeltet ikke er udfyldt.
	 */
	private void salgAction() {
		String medarbejder = txfMedarbejder.getText().trim();
		if (medarbejder.length() < 1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Salg");
			alert.setHeaderText("Medarbejder mangler");
			alert.showAndWait();
		} else {
			SalgsVindue sv = new SalgsVindue("Salg", medarbejder, cbxPrisliste.getSelectionModel().getSelectedItem());
			sv.showAndWait();
		}
	}

	/**
	 * TODO i version 1.1
	 */
	private void orgAction() {
		//TODO
	}
}
