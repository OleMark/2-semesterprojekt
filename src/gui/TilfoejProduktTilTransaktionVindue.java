package gui;

import java.util.ArrayList;
import java.util.List;

import application.controller.Controller;
import application.model.Prisliste;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Transaktion;
import application.model.TransaktionsLinje;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Et vindue hvor man kan tilfoeje et eller flere produkter til transaktionen
 * 
 * @author Oliver, Jeppe, Thor, Ole
 * @version 1.0
 */

public class TilfoejProduktTilTransaktionVindue extends Stage {

	// Transaktion der er oprettet i salgsvinduet
	private Transaktion transaktion;
	// Prisliste der er valgt i MainAppen
	private Prisliste prisliste;
	// Listen hvor transaktionslinjerne midlertidigt bliver tilfoejet indtil man
	// lukker dette vindue
	private List<TransaktionsLinje> transaktionslinjer = new ArrayList<>();

	public TilfoejProduktTilTransaktionVindue(String title, Transaktion transaktion, Prisliste prisliste) {

		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setResizable(false);

		this.transaktion = transaktion;
		this.prisliste = prisliste;

		setTitle(title);
		GridPane pane = new GridPane();
		initContent(pane);

		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private ListView<Produkt> lvwProdukter;
	private ListView<TransaktionsLinje> lvwTransaktionsLinjer;
	private ComboBox<Produktgruppe> cbxProduktgruppe;
	private TextField txfAntal, txfPris;

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(20);
		pane.setGridLinesVisible(false);

		Label lblProduktgruppe = new Label("Produktgruppe:");
		pane.add(lblProduktgruppe, 0, 0);
		GridPane.setValignment(lblProduktgruppe, VPos.BOTTOM);

		cbxProduktgruppe = new ComboBox<>();
		cbxProduktgruppe.getItems().addAll(Controller.getProduktgrupper());
		pane.add(cbxProduktgruppe, 0, 1);
		cbxProduktgruppe.getSelectionModel().select(0);
		ChangeListener<Produktgruppe> listener = (ov, oldListe, newListe) -> this.selectedListChanged();
		cbxProduktgruppe.getSelectionModel().selectedItemProperty().addListener(listener);

		Label lblProdukter = new Label("Produkter");
		pane.add(lblProdukter, 0, 2);
		GridPane.setValignment(lblProdukter, VPos.BOTTOM);

		lvwProdukter = new ListView<>();
		pane.add(lvwProdukter, 0, 3, 2, 5);
		lvwProdukter.setPrefHeight(200);
		selectedListChanged();
		lvwProdukter.getSelectionModel().select(0);
		ChangeListener<Produkt> produktlistener = (ov, oldProdukt, newProdukt) -> selectedProduktChanged();
		lvwProdukter.getSelectionModel().selectedItemProperty().addListener(produktlistener);

		Label lblAntal = new Label("Antal");
		pane.add(lblAntal, 2, 2);

		txfAntal = new TextField();
		pane.add(txfAntal, 2, 3);

		Label lblPris = new Label("Pris");
		pane.add(lblPris, 2, 4);

		txfPris = new TextField();
		pane.add(txfPris, 2, 5);

		Button btnTilfoej = new Button("Tilføj");
		pane.add(btnTilfoej, 2, 6);
		btnTilfoej.setPrefSize(75, 40);
		GridPane.setValignment(btnTilfoej, VPos.CENTER);
		GridPane.setHalignment(btnTilfoej, HPos.CENTER);
		btnTilfoej.setOnAction(e -> tilfoejAction());

		Button btnFjern = new Button("Fjern");
		pane.add(btnFjern, 2, 7);
		btnFjern.setPrefSize(75, 40);
		GridPane.setValignment(btnFjern, VPos.TOP);
		GridPane.setHalignment(btnFjern, HPos.CENTER);
		btnFjern.setOnAction(e -> fjernAction());

		Label lblTilfoejedeProdukter = new Label("Tilføjede produkter");
		pane.add(lblTilfoejedeProdukter, 3, 2);

		lvwTransaktionsLinjer = new ListView<>();
		pane.add(lvwTransaktionsLinjer, 3, 3, 2, 5);
		lvwProdukter.setPrefHeight(200);

		Button btnOK = new Button("OK");
		pane.add(btnOK, 3, 8);
		btnOK.setOnAction(e -> OKAction());
		GridPane.setHalignment(btnOK, HPos.RIGHT);

		Button btnAnnuller = new Button("Annuller");
		pane.add(btnAnnuller, 4, 8);
		btnAnnuller.setTextAlignment(TextAlignment.CENTER);
		GridPane.setHalignment(btnAnnuller, HPos.RIGHT);
		btnAnnuller.setOnAction(e -> annullerAction());

		selectedProduktChanged();

	}

	/**
	 * Opdaterer listViewet over produkter ud fra hvilken produktgruppe man har
	 * valgt
	 */

	private void selectedListChanged() {
		lvwProdukter.getItems().setAll(cbxProduktgruppe.getSelectionModel().getSelectedItem().getProdukter());
	}

	/**
	 * Tilfoejer et produkt, pris og antal til en transaktionslinje Gemmer
	 * transaktionslinjen i Storage samt i den midlertidige liste Nulstiller antal
	 * og pris i GUI'en, efter et produkt er tilfoejet til transaktionslinjen
	 */

	private void tilfoejAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			return;
		}

		double pris = 0.0;
		try {
			pris = Double.parseDouble(txfPris.getText());
			if (pris < 0.0) {
				throw new NumberFormatException();
			}

		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Tilføj produkt");
			alert.setHeaderText("Pris skal være større end eller lig med 0");
			alert.showAndWait();
			return;
		}

		int antal = 1;
		try {
			antal = Integer.parseInt(txfAntal.getText());
			if (antal < 1) {
				throw new NumberFormatException();
			}

		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Tilføj produkt");
			alert.setHeaderText("Antal skal være større end 0");
			alert.showAndWait();
			return;
		}

		TransaktionsLinje tl = Controller.opretTransaktionsLinje(transaktion, antal, produkt, prisliste);

		if (tl.getPris() != pris) {
			Controller.setPrisPaaTransaktionsLinje(tl, pris, transaktion);
		}

		transaktionslinjer.add(tl);

		transaktionsLinjerChanged();
		selectedProduktChanged();
	}

	/**
	 * Fjerner en tilfoejet transaktionslinje fra transaktionen Nulstiller antal og
	 * pris i GUI'en, efter en transaktionslinje er fjernet fra transaktionen
	 */

	private void fjernAction() {
		TransaktionsLinje tl = lvwTransaktionsLinjer.getSelectionModel().getSelectedItem();

		if (tl == null) {
			return;
		}

		Controller.fjernTransaktionsLinjeFraTransaktion(tl, transaktion);

		transaktionslinjer.remove(tl);

		transaktionsLinjerChanged();
		selectedProduktChanged();

	}

	/**
	 * Lukker dette vindue
	 */

	private void OKAction() {
		close();
	}

	/**
	 * Fjerne alle midlertidigt tilfoejede transaktionslinjer fra storage Lukker
	 * dette vindue
	 */

	private void annullerAction() {
		Controller.fjernTransaktionsLinjeListeFraTransaktion(transaktionslinjer, transaktion);
		close();
	}

	/**
	 * Opdaterer hvilket produkt der er valgt, samt saetter prisen ind i teksfeltet
	 * og seatter antal til standard "1"
	 */

	private void selectedProduktChanged() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		txfPris.setText(Controller.findPrisPaaProduktIPrisliste(prisliste, produkt) + "");
		txfAntal.setText("1");
	}

	/**
	 * Opdaterer listViewet med transaktionslinjer
	 */

	private void transaktionsLinjerChanged() {
		lvwTransaktionsLinjer.getItems().setAll(transaktionslinjer);
	}

}
